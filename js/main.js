var map = Snap("#map");

var path = {
    "lviv" : "M113.853 163.182C253.688 154.63 322.327 179.073 433.338 256.871",
    "kyiv" : "M370 143C423.68 158.579 435.983 185.543 440 252",
    "harkiv" : "M605 167C605 167 558 180 519.5 201.5C481 223 445 257 445 257",
    "dnipro" : "M512 305.5C512 305.5 494 292 480.5 282C467 272 446.5 263.5 446.5 263.5",
    "odesa" : "M383.5 387.5C383.5 387.5 386 346.5 396.5 322C407 297.5 437.5 264.5 437.5 264.5",
};

$('svg path').on('click', function () {
    var getId = $(this).attr('id');
    $('path').removeClass('active_city');
    $(this).addClass('active_city');
    $('circle, .name_city').hide();
    $('ellipse').removeClass('active_dot');
    $('#' + getId + '_round').addClass('active_dot');
    $('#' + getId + '_name').fadeIn();
    $('#' + getId + '_pulse').fadeIn();
    $('.lineActive').hide();
    commonFunc(getId);
});

function commonFunc(getId) {
    var linePath = path[getId];
    var lineLength = Snap.path.getTotalLength(linePath);
    var lineDraw = map.path(linePath);

    lineDraw.attr({
        fill:'none',
        stroke:'#272038',
        'stroke-dasharray': lineLength + ' ' + lineLength,
        'stroke-dashoffset': lineLength,
        'stroke-width': 2,
        'stroke-linecap': 'round',
        'stroke-linejoin': 'round',
    });
    lineDraw.addClass('lineActive');
    lineDraw.animate({
        strokeDashoffset : 0
    },200, mina.linear)
}
